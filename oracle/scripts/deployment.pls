/*
 * Execute this deployment script from the current directory
 * in which the source files are located.
 *
 * These files can also be placed in the SQLPATH folder, 
 * which can be found by issuing: 
 *   - "echo $SQLPATH" on UNIX/Linux
 *   - "echo %SQLPATH%" on Windows.
 * A list of all environment variables for Linux (Windows) 
 * can be obtained by the command "printenv" ("set").
 *
 * To find the present working directory, use "host pwd" 
 * in SQL*Plus.
 * Note that "host cd" in SQL*Plus creates a subshell, 
 * which does not affect SQL*Plus itself.
 */
@"tables/error_log.sql"
/
@"packages/errors.pks"
/
@"packages/errors.pkb"
/
@"/packages/type_defs.pkg"
/
@"packages/plsql_utils.pks"
/
@"packages/plsql_utils.pkb"
/
@"packages/sql_utils.pks"
/
@"packages/sql_utils.pkb"
/
@"views/data_type_issues.sql"
/
