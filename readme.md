# Code repository for databaseline.wordpress.com

This repository contains code mentioned and used in technical posts on my technical blog [Databaseline](http//databaseline.wordpress.com).

## Posts

The files are organized into different folders based on the language/technology used:

* `julia`: [Julia](http://julialang.org) for analytics and data science.
* `maple`: [Maple](http://www.maplesoft.com) for technical (i.e. mathematical) computing.
* `oracle`: Oracle Database, 'nough said.
* `r`: [R](http://www.r-project.org) for statistics and analytics.

Below is a list of available files and blog posts to which they belong.

Folder | File                                     | Post(s)
:-----:|:---------------------------------------- |:-------------------------------------------------------------------------------------------------------------
`maple`  | variational integrators.src              | [Numerical Algorithms: Variational Integration](http://wp.me/p4zRKC-2O)
`oracle` | DOC - oracle guidelines footer.html      | [Oracle SQL and PL/SQL Coding Guidelines](http://wp.me/p4zRKC-3k)
`oracle` | DOC - oracle guidelines.md               | [Oracle SQL and PL/SQL Coding Guidelines](http://wp.me/p4zRKC-3k)
`oracle` | DOC - oracle guidelines style.css        | [Oracle SQL and PL/SQL Coding Guidelines](http://wp.me/p4zRKC-3k)
`oracle` | FUNCTION - eval.pls                      | How to Multiply Across a Hierarchy in Oracle [Part 1](http://wp.me/p4zRKC-2B) & [Part 2](http://wp.me/p4zRKC-2G)
`oracle` | FUNCTION - run_mult_stmt.pls             | How to Multiply Across a Hierarchy in Oracle [Part 2](http://wp.me/p4zRKC-2G)
`oracle` | PACKAGE - errors.pks                     | [Searching The Oracle Data Dictionary](http://wp.me/p4zRKC-2U)
`oracle` | PACKAGE - errors.pkb                     | [Searching The Oracle Data Dictionary](http://wp.me/p4zRKC-2U)  
`oracle` | PACKAGE - plsql_utils.pks                | [Searching The Oracle Data Dictionary](http://wp.me/p4zRKC-2U) & [Checking Data Type Consistency in Oracle](http://wp.me/p4zRKC-42) 
`oracle` | PACKAGE - plsql_utils.pkb                | [Searching The Oracle Data Dictionary](http://wp.me/p4zRKC-2U) & [Checking Data Type Consistency in Oracle](http://wp.me/p4zRKC-42) 
`oracle` | PACKAGE - sql_utils.pks                  | [Checking Data Type Consistency in Oracle](http://wp.me/p4zRKC-42) 
`oracle` | PACKAGE - sql_utils.pkb                  | [Checking Data Type Consistency in Oracle](http://wp.me/p4zRKC-42) 
`oracle` | PACKAGE - type_defs.pkg                  | [Searching The Oracle Data Dictionary](http://wp.me/p4zRKC-2U) & [Checking Data Type Consistency in Oracle](http://wp.me/p4zRKC-42)
`oracle` | PROCEDURE - prepare_table.pls            | How to Multiply Across a Hierarchy in Oracle [Part 1](http://wp.me/p4zRKC-2B) & [Part 2](http://wp.me/p4zRKC-2G)
`oracle` | SCRIPT - deployment                      | [Checking Data Type Consistency in Oracle](http://wp.me/p4zRKC-42)
`oracle` | SCRIPT - multiply across hierarchy.pls   | How to Multiply Across a Hierarchy in Oracle [Part 2](http://wp.me/p4zRKC-2G)
`oracle` | SCRIPT - multiply across hierarchy.sql   | How to Multiply Across a Hierarchy in Oracle [Part 1](http://wp.me/p4zRKC-2B)
`oracle` | TABLE - error_log.sql                    | [Searching The Oracle Data Dictionary](http://wp.me/p4zRKC-2U) & [Checking Data Type Consistency in Oracle](http://wp.me/p4zRKC-42)
`oracle` | VIEW - data_type_issues.sql              | [Checking Data Type Consistency in Oracle](http://wp.me/p4zRKC-42)
`r`      | kaggle nyt articles.R                    | What Makes a New York Times Article Popular? [Part 1](http://wp.me/p4zRKC-4i) & [Part 2](http://wp.me/p4zRKC-4k)


The files in the `oracle` folder all follow the same naming convention. For instance, `PACKAGE - package_name.pkg` can be found in the `packages` folder and the file is called `package_name.pkg`. The object types have been added, so as to make it easier to navigate the source.

Please note that some posts may not yet be online because they are in the queue to be published. The code for these posts may, however, already be available from this repository.

Compatibility notes in the header of each package specification indicate the Oracle Database versions on which the code can be compiled successfully, or on which the code can be compiled successfully with minor modifications. The availability of features that require newer versions is listed in the header files as well, so as to make adjustments easy.

## File Types

The following is a list of file extensions used on this repository for code pertaining to Oracle Database.

 Extension  | Contents
:----------:|:---------------------------------------
 .sql       | SQL code or script
 .pls       | PL/SQL script, function, or procedure
 .pks       | Package specification
 .pkb       | Package body
 .pkg       | Package specification (without body)

## Disclaimer

You are free to use and/or adapt all code available on this repository without any restrictions. Neither the author(s) of the code nor the owner of the repository, Ian Hellström, are, however, responsible for any damages due to the use or adaptation of said code. All code is provided as is without any warranty.
